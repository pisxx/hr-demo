HR Demo Project

1. Terraform
- Plan
terraform plan -out=tf.plan
- Apply
terraform apply "tf.plan"
- Show manager resources
terraform state list
- Plan
terraform plan -out=tf.plan
- Apply
terraform apply "tf.plan"
2. Ansible
- apply playbook x2
ansible-playbook -i hosts playbook.yml
3. Deploy K8s
export NAME=demo.k8s.local
export KOPS_STATE_STORE=s3://pslawek-kops

kops create cluster --zones us-east-1a,us-east-1b,us-east-1c $NAME
kops create cluster --config cluster.yml --zones us-east-1a,us-east-1b,us-east-1c $NAME
ssh-keygen
kops create secret --name $NAME sshpublickey admin -i ~/.ssh/id_rsa.pub

kops update cluster $NAME --yes
4. Terragorm
- Add resource
- change provisioner settings
- Plan
- Apply
5. Ansible
- apply playbook x2
6. Docker
git checkout -b feature2
apply new stuff to webpage
git checkout master
git merge feature2
7. K8s
- Deploy to workload to k8s
kubectl apply -f .
- show config
kubectl get pods
kubectl get svc
- Kill Pod
kubect delete pod
- Increase Pods
- Kill Node
kubctl get pods -o wide
aws kill node
kubectl get pods -o wide
8. Destroy K8s cluster using kops
kops delete cluster --name $NAME --yes
9. Destroy env using Terraform
