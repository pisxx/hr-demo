resource "aws_instance" "kops-demo" {
    # ami                          = "ami-062f7200baf2fa504"
    ami                          = data.aws_ami.amzn.id
    associate_public_ip_address  = true
    availability_zone            = "us-east-1a"
    iam_instance_profile         = "kops"
    instance_type                = "t2.micro"
    key_name                     = "aws-interia"
    monitoring                   = false
    # primary_network_interface_id = "eni-0c7f7d2c8d2f18196"
    # security_groups              = [
    #     "admin_sg",
    # ]
    source_dest_check            = true
    subnet_id                    = "subnet-de3e3bf3"
    tags                         = {
        "Name" = "kops-demo"
    }
    vpc_security_group_ids       = [
        "sg-0baf19d7ecb2dc0a7",
    ]

    # credit_specification {
    #     cpu_credits = "standard"
    # }

    root_block_device {
        delete_on_termination = true
        encrypted             = false
        iops                  = 100
        # volume_id             = "vol-0b919fce7c7c8cb72"
        volume_size           = 8
        volume_type           = "gp2"
    }


    timeouts {}

    provisioner "local-exec" {
      command = "echo '[demo]\nkops-demo\tansible_host=${aws_instance.kops-demo.public_ip}' > ../ansible/hosts"
    }
}

# resource "aws_instance" "kops-demo2" {
#     # ami                          = "ami-062f7200baf2fa504"
#     ami                          = data.aws_ami.amzn.id
#     associate_public_ip_address  = true
#     availability_zone            = "us-east-1a"
#     iam_instance_profile         = "kops"
#     instance_type                = "t2.micro"
#     key_name                     = "aws-interia"
#     monitoring                   = false
#     # primary_network_interface_id = "eni-0c7f7d2c8d2f18196"
#     # security_groups              = [
#     #     "admin_sg",
#     # ]
#     source_dest_check            = true
#     subnet_id                    = "subnet-de3e3bf3"
#     tags                         = {
#         "Name" = "kops-demo"
#     }
#     vpc_security_group_ids       = [
#         "sg-0baf19d7ecb2dc0a7",
#     ]

#     # credit_specification {
#     #     cpu_credits = "standard"
#     # }

#     root_block_device {
#         delete_on_termination = true
#         encrypted             = false
#         iops                  = 100
#         # volume_id             = "vol-0b919fce7c7c8cb72"
#         volume_size           = 8
#         volume_type           = "gp2"
#     }


#     timeouts {}

#     provisioner "local-exec" {
#       command = "echo 'kops-demo2\tansible_host=${aws_instance.kops-demo2.public_ip}' >> ../ansible/hosts"
#     }
# }