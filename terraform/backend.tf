terraform {
  backend "s3" {
    region         = "us-east-1"
    bucket         = "pisxx-hr-demo"
    key            = "terraform/us-east-1/hr-demo.tfstate"
    dynamodb_table = "terraform-locks"
    # shared_credentials_file = "~/.aws/credentials"
    shared_credentials_file = "~/.aws/credentials"
  }
}